<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //add role
        $roles = [
            [
                'name' => 'admin',
                'display_name' => 'Admin',
                'description' => 'Only one and only admin',
            ],
            [
                'name' => 'pimpinan',
                'display_name' => 'Pimpinan',
                'description' => 'Access for registed user',
            ],
        ];

        foreach ($roles as $key => $value) {
            Role::create($value);
        }
        //add user
        $users = [
            [
                'name' => 'Widhy Arsana',
                'email' => 'widhyarsana@gmail.com',
                'password' => bcrypt('123456'),
            ],
            [
                'name' => 'Rizky Aditya',
                'email' => 'rizkyaditya@gmail.com',
                'password' => bcrypt('123456'),
            ],
        ];
        $n=1;
        foreach ($users as $key => $value) {
            $user=User::create($value);
            $user->attachRole($n);
            $n++;
        }
    }
}
